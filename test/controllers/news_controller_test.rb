require 'test_helper'

class NewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @news = news(:one)
  end

  test "should get index" do
    get news_index_url, as: :json
    assert_response :success
  end

  test "should create news" do
    assert_difference('News.count') do
      post news_index_url, params: { news: { course_id: @news.course_id, image_content_type: @news.image_content_type, image_file_name: @news.image_file_name, image_file_size: @news.image_file_size, image_updated_at: @news.image_updated_at, news_autor: @news.news_autor, news_content: @news.news_content, news_title: @news.news_title, section_id: @news.section_id } }, as: :json
    end

    assert_response 201
  end

  test "should show news" do
    get news_url(@news), as: :json
    assert_response :success
  end

  test "should update news" do
    patch news_url(@news), params: { news: { course_id: @news.course_id, image_content_type: @news.image_content_type, image_file_name: @news.image_file_name, image_file_size: @news.image_file_size, image_updated_at: @news.image_updated_at, news_autor: @news.news_autor, news_content: @news.news_content, news_title: @news.news_title, section_id: @news.section_id } }, as: :json
    assert_response 200
  end

  test "should destroy news" do
    assert_difference('News.count', -1) do
      delete news_url(@news), as: :json
    end

    assert_response 204
  end
end
