# Specifications

## URL
[https://pseesapinews.herokuapp.com]

## Gems includes

* 'rails'
* 'paperclip'
* 'aws-sdk'
* 'typhoeus'

### Notes

aws-sdk is to connect to amazon Services

## Services

1. [News](#1-news)
*  [sections](#2-sections)

## Routes dictionary

### **1. News**

* **get** ~ */news*
Retrives data list about all the news in the database

Responses:
```
[{"id": "News id", "news_title":"News title", "news_content":"Content of the new", "news_autor":"Autor of the new", "image_file_name": Name of the image saved, "image_url": "url of the image"}, ...]
```

* **get** ~ */news/:id*
Retrives the data from a single list

Receives:
```
{"id":Identifier of the news}
```

Responses:
```
{"id": "News id", "news_title":"News title", "news_content":"Content of the new", "news_autor":"Autor of the new", "image_file_name": Name of the image file, "image_url": "url of the image"}
```

* **post** ~ */news*
Create a new register of news

Receives:
```
{"news_title":"News title", "news_content":"Content of the new", "news_autor":"Autor of the new", "image_file_name": "Name of the image file","image_encoded":"Base 64 image encoded","section_id":"Section Identifier reference" , "course_id":"Course Identifier reference"}
```

Responses:
```
{"id": "News id", "news_title":"News title", "news_content":"Content of the new", "news_autor":"Autor of the new", "image_file_name": "Name of the image saved", "image_url": "url of the image"}
```

* **patch/put** ~ */news/:id*
Update the data from a single news

Receives:
```
{"id":"Identifier of the news","news_title":"News title", "news_content":"Content of the new", "news_autor":"Autor of the new", "image_file_name": "Name of the image","image_encoded":"Base 64 image encoded","section_id":"Section Identifier reference" , "course_id":"Course Identifier reference"}
```

Responses:
```
status 200: All ok!
or
status 400: Something went wrong
```

* **delete** ~ */news/id*
Delete a register of a single news

Receives:
```
{"id":"Id of the news"}
```

Responses:
```
status 200: News deleted!
of
status 400: News not found
```

### **2. Sections**

* **get** ~ */sections*
