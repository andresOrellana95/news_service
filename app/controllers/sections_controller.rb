class SectionsController < ApplicationController
  #before_action :course_admin, except: [:show]
  #before_action :any_user, only: [:show]
  # GET /sections/1


  def show
    #Returns a single object of section
    @section = Section.find(params[:id])
    render json: @section
  end

  # POST /sections
  def create
    @section = Section.new(section_params)
    if @section.save
      #Returns a section un json object
      render json: @section, status: :created, location: @section
    else
      #Returns a stacktrace
      render json: @section.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /sections/1
  def update
    @section = Section.find(params[:id])
    if @section.update(section_params)
      #Returns section in json object
      render json: @section
    else
      #Returns error message
      render json: @section.errors, status: :unprocessable_entity
    end
  end

  # DELETE /sections/1
  def destroy
    begin
      @section = Section.find(params[:id])
      @section.destroy
      #Returns de success message of the process
      render :json => {:sucess => ["Section deleted!"]}, status: 200
    rescue ActiveRecord::RecordNotFound
      #Returns not found message
      render :json => {:errors => ["Section not found"]}, status: 404
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def section_params
      params.require(:section).permit(:section_name, :section_description)
    end
end
