class ApplicationController < ActionController::API
=begin
[

  attr_reader :current_user

  private

  def authenticate_request
    if request.headers['Authorization'].present?
      @token = request.headers['Authorization'].split(' ').last
      @current_user = auth_api(@token)
      render json: {error: 'Not Authorized'}, status: 401 unless @current_user
    else
      render json: {error: 'No token in headers'}
    end
  end

  def auth_api(token)
    response = Typhoeus.get("http://pseesapiauth.herokuapp.com/check?token="+token)
    user = JSON.parse(response.body)
    if !user["error"]
      return user
    else
      return nil
    end
  end

  def course_admin
    authenticate_request
    if(@current_user && !@current_user["permissions"].include?("course_admin"))
      render json: {error: 'Forbidden'}, status: 403
    end
  end

  def any_user
    authenticate_request
    if(@current_user && !@current_user["permissions"].include?("course_admin") && !@current_user["permissions"].include?("student"))
      render json: {error: 'Forbidden'}, status: 403
    end
  end
]
=end
end
