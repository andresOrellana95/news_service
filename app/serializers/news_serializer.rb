
class NewsSerializer < ActiveModel::Serializer
  attributes :id, :news_title, :news_content, :news_autor, :image_file_name, :image_url
end
