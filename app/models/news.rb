class News < ApplicationRecord
  belongs_to :section
  belongs_to :course

  has_attached_file :image, styles: {thumb: '100x100>'}
  validates_attachment_content_type :image, :content_type => ["image/jpg","image/jpeg","image/png","image/gif"]
  validates_attachment_presence :image

  def image_url
    image.url(:thumb)
  end
end
