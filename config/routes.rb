Rails.application.routes.draw do
  #News routes
  post "news", to: "news#create"
  get "news", to: "news#show_all"
  get "news/:id", to: "news#show"
  patch "news/:id", to: "news#update_new"
  put "news/:id", to: "news#update_new"
  delete "news/:id", to: "news#destroy"
  #Sections routes
  post "sections", to: "sections#create"
  get "sections/:id", to: "sections#show"
  put "sections/:id", to: "sections#update"
  patch "sections/:id", to: "sections#update"
  delete "sections/:id", to: "sections#destroy"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
