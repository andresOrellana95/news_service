# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_stat_statements"
  enable_extension "dblink"
  enable_extension "xml2"
  enable_extension "ltree"
  enable_extension "pgstattuple"
  enable_extension "tablefunc"
  enable_extension "fuzzystrmatch"
  enable_extension "citext"
  enable_extension "uuid-ossp"
  enable_extension "pgcrypto"
  enable_extension "pg_trgm"
  enable_extension "intarray"
  enable_extension "hstore"
  enable_extension "dict_int"
  enable_extension "btree_gin"
  enable_extension "pgrowlocks"
  enable_extension "cube"
  enable_extension "btree_gist"
  enable_extension "dict_xsyn"
  enable_extension "plperl"

  create_table "answers", force: :cascade do |t|
    t.integer "question_id",                 null: false
    t.string  "answer_statement", limit: 80, null: false
    t.boolean "correct_answer",              null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string "course_name",      limit: 30, null: false
    t.string "course_objective", limit: 50
  end

  create_table "evaluations", force: :cascade do |t|
    t.integer "module_id",                                          null: false
    t.integer "evaluation_time",                       default: 60, null: false
    t.integer "evaluation_q_link_questions", limit: 2, default: 1,  null: false
  end

  create_table "material_types", force: :cascade do |t|
    t.string   "material_type_name", limit: 30
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.index ["material_type_name"], name: "material_type_name_unique", unique: true, using: :btree
  end

  create_table "modules", force: :cascade do |t|
    t.integer "course_id",                                  null: false
    t.string  "module_name",      limit: 30,                null: false
    t.integer "module_level",     limit: 2,                 null: false
    t.boolean "module_active",               default: true, null: false
    t.string  "module_objective", limit: 50
  end

  create_table "news", force: :cascade do |t|
    t.integer  "section_id",                      null: false
    t.integer  "course_id"
    t.string   "news_title",         limit: 30,   null: false
    t.string   "news_content",       limit: 1024, null: false
    t.string   "news_autor",         limit: 30
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "questions", force: :cascade do |t|
    t.integer "evaluation_id",                   null: false
    t.string  "question_statement",   limit: 80, null: false
    t.string  "question_type",        limit: 8,  null: false
    t.integer "multiple_question_id", limit: 2
  end

  create_table "sections", force: :cascade do |t|
    t.string "section_name",        limit: 30, null: false
    t.string "section_description", limit: 50, null: false
    t.index ["section_name"], name: "sections_name_unique", unique: true, using: :btree
  end

  create_table "students", force: :cascade do |t|
    t.integer "course_id",                            null: false
    t.string  "correo",        limit: 50,             null: false
    t.integer "student_level", limit: 2,  default: 1, null: false
  end

  create_table "study_materials", force: :cascade do |t|
    t.integer  "module_id",                         null: false
    t.integer  "study_material_type_id",            null: false
    t.string   "study_material_name",    limit: 30, null: false
    t.string   "material_file_name"
    t.string   "material_content_type"
    t.integer  "material_file_size"
    t.datetime "material_updated_at"
  end

  add_foreign_key "answers", "questions", name: "answer_question_fk"
  add_foreign_key "evaluations", "modules", name: "evaluation_module_fk"
  add_foreign_key "modules", "courses", name: "modules_course_fh"
  add_foreign_key "news", "courses", name: "news_course_fk"
  add_foreign_key "news", "sections", name: "news_section_fk"
  add_foreign_key "questions", "evaluations", name: "question_evaluation_fk"
  add_foreign_key "students", "courses", name: "student_course_fk"
  add_foreign_key "study_materials", "material_types", column: "study_material_type_id", name: "study_material_type_fk"
  add_foreign_key "study_materials", "modules", name: "study_material_module_fk"
end
